var precss       = require('precss');
var autoprefixer = require('autoprefixer');

module.exports = {
    entry: "./assets/jsx/app.jsx",
    output: {
        path: __dirname,
        filename: "./public/js/bundle.js"
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.css', '.scss']
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                    presets: ['react', 'es2015']
                }
            },
            { 
                test: /\.scss$/,
                loaders: [ 'style', 'css?sourceMap', 'postcss', 'sass?sourceMap' ]
            }         
        ]
    },
    sassLoader: {
        includePaths: [ './assets/scss']
    },
    postcss: function () {
        return {
            defaults: [precss, autoprefixer],
            cleaner:  [autoprefixer({ browsers: ['ie9-11', '> 1%'] })]
        };
    }
};