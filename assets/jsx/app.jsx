import "../scss/style";
import React from 'react';
import ReactDOM from 'react-dom';
import WorkoutComponent from './WorkoutComponent/WorkoutComponent';

/**
 * Render the App.
 *
 * @version 1.0.0
 */
ReactDOM.render(
    <div className="app">
        <WorkoutComponent/>
    </div>, 
    document.getElementById('app')
);