import React from 'react';

/**
 * Button Class
 *
 * This class represents a UI Button.
 *
 * @version 1.0.0
 */
class Button extends React.Component {

    /**
     * Class Constructor 
     *
     * @param mixed props immutable states object.
     * @return void
     * @public
     */
    constructor(props) {
        super(props);
    }

    /**
     * Render the UI Object.
     *  
     * @return mixed
     * @public
     */
    render() {

        if(!this.props.isDisable)
        {
            return(
                <button onClick={this.props.clicked} className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" disabled>{this.props.text}</button>
            );
        }
        else
        {
            return(
                <button onClick={this.props.clicked} className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">{this.props.text}</button>
            );
        }  
    }
}

export default Button;