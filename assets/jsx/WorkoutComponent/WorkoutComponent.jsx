import React from 'react';
import WorkoutForm from './WorkoutForm';
import WorkoutList from './WorkoutList';
import WorkoutResult from './WorkoutResult';

/**
 * WorkoutComponent Class
 *
 * This class represents a WorkoutComponent.
 *
 * @author Bruno Lebtag <brlebtag@gmail.com>
 * @version 1.0.0
 */
class WorkoutComponent extends React.Component {

    /**
     * Class Constroctor 
     *
     * Set up initial states and fix `this` variable in es6 Class
     *
     * @param mixed props immutable states object.
     * @return void
     * @public
     */
    constructor(props) {
        super(props);

        this.state = {
            workouts: [],
            result: 0,
        }

        // Fixing `this` variable in es6 class 
        this.newWorkout = this.newWorkout.bind(this);
        this.removeWorkout = this.removeWorkout.bind(this);
        this.timeSpentSort = this.timeSpentSort.bind(this);
        this.typeSort = this.typeSort.bind(this);
        this.dateSort = this.dateSort.bind(this);
        this.updateResult = this.updateResult.bind(this);
    }

    /**
     * Handle onNewWorkOut event
     *
     * @param mixed workout workout object.
     * @return void
     * @public
     */
    newWorkout(workout) {
        this.setState({ workouts: this.__append(workout) });
        this.updateResult(this.state.result + workout.time_spent);
    }

    /**
     * Handle onRemoveWorkOut event
     *
     * @param int index workout index to be removed.
     * @return void
     * @public
     */
    removeWorkout(index) {
        var result = this.state.result - this.state.workouts[index].time_spent;
        this.setState({ workouts: this.__remove(index) });
        this.updateResult(result);
        
    }

    /**
     * Handle onTimeSpentSort event
     *
     * @param int order 0 - do not reverse and 1 - reverse.
     * @return void
     * @public
     */
    timeSpentSort(order) {
        this.setState({ workouts: this.__sortBy('time_spent', order) });
    }

    /**
     * Handle onTypeSort event
     *
     * @param int order 0 - do not reverse and 1 - reverse.
     * @return void
     * @public
     */
    typeSort(order) {
        this.setState({ workouts: this.__sortBy('type', order) });
    }

    /**
     * Handle onDateSort event
     *
     * @param int order 0 - do not reverse and 1 - reverse.
     * @return void
     * @public
     */
    dateSort(order) {
        this.setState({ workouts: this.__sortBy('date', order) });
    }

    /**
     * Updates Result.
     *
     * @param int result number of hours the person was exercising.
     * @return void
     * @public
     */
    updateResult(result)
    {
        this.setState({ result: result});
    }

    /**
     * Render the UI Object.
     *  
     * @return mixed
     * @public
     */
    render() {
        return(
            <div className="Workout-component">
                <WorkoutForm text="New Workout Log" onNewWorkout={this.newWorkout}/>
                <WorkoutList workouts={this.state.workouts} onRemoveWorkout={this.removeWorkout} onTimeSpentSort={this.timeSpentSort} onTypeSort={this.typeSort} onDateSort={this.dateSort} text="Workout Logs"/>
                <br/><br/>
                <WorkoutResult result={this.state.result}/>
            </div>
        );
    }
    
    /**
     * Append workout to this.state.workouts
     * and return the new array.
     *  
     * @param workout object to be appended to this.state.workouts
     * @return array
     * @private
     */
    __append(workout) {
        var workouts = this.state.workouts;
        workouts.push(workout);
        return workouts;
    }

    /**
     * Remove workout in the `index` position
     * and return the new array.
     *  
     * @param index index position to be removed
     * @return array
     * @private
     */
    __remove(index) {
        var workouts = this.state.workouts;
        workouts.splice(index, 1);
        return workouts;
    }

    /**
     * Sort this.state.workouts based on `key` and
     * reverse the array if `order` is true.
     *  
     * @param int order 0 - do not reverse, 1 - reverse
     * @param string key key to be sorted by.
     * @return array
     * @private
     */
    __sortBy(key, order) {
        var workouts = this.state.workouts.sort(function(a, b) {

            if(a[key] < b[key])
                return -1;
            else if (a[key] > b[key]) 
                return 1;
            else
                return 0;
        });

        if(order)
            return workouts.reverse();
        return workouts;
    }
}

export default WorkoutComponent;