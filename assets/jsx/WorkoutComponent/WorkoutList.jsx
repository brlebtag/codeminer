import React from 'react';

/**
 * WorkoutList Class
 *
 * This class represents a list of workouts.
 *
 * @author Bruno Lebtag <brlebtag@gmail.com>
 * @version 1.0.0
 */
class WorkoutList extends React.Component {
    
    /**
     * Class Constructor 
     *
     * Set up initial states and fix `this` variable in es6 Class
     *
     * @param mixed props immutable states object.
     * @return void
     * @public
     */
    constructor(props) {
        super(props);

        this.state = {
            time_spent: 1,
            type: 1,
            date: 1
        }

        // Fixing `this` variable in es6 class 
        this.onTimeSpentSort = this.onTimeSpentSort.bind(this);
        this.onTypeSort = this.onTypeSort.bind(this);
        this.onDateSort = this.onDateSort.bind(this);
    }

    /**
     * Handle remove workout events
     *
     * @param int index workout index to be removed.
     * @return void
     * @public
     */
    onRemoveWorkout(index) {
        this.props.onRemoveWorkout(index);
    }

    /**
     * Handle time spent sort event
     *
     * @return void
     * @public
     */
    onTimeSpentSort() {
        var result = 1 - this.state.time_spent;
        this.setState({time_spent: result});
        this.props.onTimeSpentSort(result);
    }

    /**
     * Handle type sort event
     *
     * @return void
     * @public
     */
    onTypeSort() {
        var result = 1 - this.state.type;
        this.setState({type: result});
        this.props.onTypeSort(result);
    }

    /**
     * Handle date sort event
     *
     * @return void
     * @public
     */
    onDateSort() {
        var result = 1 - this.state.date;
        this.setState({date: result});
        this.props.onDateSort(result);
    }

    /**
     * Render the UI Object.
     *  
     * @return mixed
     * @public
     */
    render() {

        var workouts = this.props.workouts.map(function(workout, index){

            return (
                <tr key={index}>
                    <td className="mdl-data-table__cell--non-numeric">{workout.time_spent}h</td>
                    <td>{workout.type}</td>
                    <td>
                        {workout.date}
                        &nbsp;&nbsp;&nbsp;
                        <button onClick={this.onRemoveWorkout.bind(this, index)} className="mdl-button mdl-js-button mdl-button--icon">
                            <i className="material-icons">delete</i>
                        </button>
                    </td>
                </tr>
            );
        }.bind(this));

        return(
            <div className="workout-list mdl-card mdl-shadow--2dp">
                <div className="mdl-card__title">
                    <h2 className="mdl-card__title-text">{this.props.text}</h2>
                </div>
                <div className="workout-list-content mdl-card__actions mdl-card--border">
                    <table className="mdl-data-table mdl-js-data-table">
                        <thead>
                            <tr>
                                <th className="mdl-data-table__cell--non-numeric">
                                    Time Spent &nbsp;
                                    <button title="Order Workout by Time Spent" onClick={this.onTimeSpentSort} className="mdl-button mdl-js-button mdl-button--icon">
                                        <i className="material-icons">sort</i>
                                    </button>
                                </th>
                                <th>
                                    Type &nbsp;
                                    <button title="Order Workout by Type" onClick={this.onTypeSort} className="mdl-button mdl-js-button mdl-button--icon">
                                        <i className="material-icons">sort</i>
                                    </button>
                                </th>
                                <th>
                                    Date &nbsp;
                                    <button title="Order Workout by Date" onClick={this.onDateSort} className="mdl-button mdl-js-button mdl-button--icon">
                                        <i className="material-icons">sort</i>
                                    </button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {workouts}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default WorkoutList;