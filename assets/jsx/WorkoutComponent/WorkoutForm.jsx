import React from 'react';
import Button from '../Button';
import WorkoutCalendar from './WorkoutCalendar';
import Moment from 'moment/moment';

/**
 * WorkoutForm Class
 *
 * This class represents a Form UI to submit a workout log
 * to be further processed by WorkoutComponent.
 *
 * @author Bruno Lebtag <brlebtag@gmail.com>
 * @version 1.0.0
 */
class WorkoutForm extends React.Component {
    
    /**
     * Class Constructor 
     *
     * Set up initial states and fix `this` variable in es6 Class
     *
     * @param mixed props immutable states object.
     * @return void
     * @public
     */
    constructor(props) {

        //Call React.Component's constructor
        super(props);

        //Setting Initial States
        this.state = {
            time_spent: 0,
            type: 'type',
            date: '',
            valid: false
        };

        // Fixing `this` variable in es6 class 
        this.onNewWorkout = this.onNewWorkout.bind(this);
        this.handleTypeSelected = this.handleTypeSelected.bind(this);
        this.handleTimeSpentSelected = this.handleTimeSpentSelected.bind(this);
        this.dateChanged = this.dateChanged.bind(this);
    }

    /**
     * Handle <select> tag `type` events
     *
     * @param mixed event event object.
     * @return void
     * @public
     */
    handleTypeSelected(event) {
        this.setState({type: event.target.value}, () => { this.__validate(); });
    }

    /**
     * Handle <select> tag `time_spent` events
     *
     * @param mixed event event object.
     * @return void
     * @public
     */
    handleTimeSpentSelected(event) {
        this.setState({time_spent: parseInt(event.target.value)}, () => { this.__validate(); });
    }

    /**
     * Handle date changes
     *
     * @param string date the new date selected.
     * @return void
     * @public
     */
    dateChanged(date) {
        this.setState({date: date}, () => { this.__validate(); });
        
    }

    /**
     * Call parent's onNewWorkout function and pass
     * the new workout log object.
     *  
     * @param mixed event event object.
     * @return void
     * @public
     */
    onNewWorkout() {
        let state_copy = this.state;
        this.__resetState();
        this.props.onNewWorkout(state_copy);
    }

    /**
     * Render the UI Object.
     *  
     * @return mixed
     * @public
     */
    render() {
        // Fill options with all possible time spent
        var options = [];

        options.push(<option key={0} value={0}>Time Spent</option>);

        for (var i = 1; i < 24; i++) {
            options.push(<option key={i} value={i}>{i}h</option>);
        }
    
        //Return the UI Object
        return(
            <div className="workout-form mdl-card mdl-shadow--2dp">
                <div className="mdl-card__title">
                    <h2 className="mdl-card__title-text">{this.props.text}</h2>
                </div>
                <div className="workout-form-content mdl-card__actions mdl-card--border">
                    <div className="centralized center">
                        <div className="column">
                            <select value={this.state.time_spent} onChange={this.handleTimeSpentSelected}>
                                {options}
                            </select>
                        </div>
                        <div className="column">
                            <select value={this.state.type} onChange={this.handleTypeSelected}>
                                <option value="type">Type</option>
                                <option value="run">Run</option>
                                <option value="swimming">Swimming</option>
                                <option value="bike">Bike</option>
                            </select>
                        </div>
                        <div className="column">
                            <WorkoutCalendar value={this.state.date} onDateChanged={this.dateChanged} />
                        </div>
                        <div className="column">
                            <Button clicked={this.onNewWorkout} text="New Log" isDisable={this.state.valid}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Reset this.state object to default values
     *
     * @return void
     * @private
     */
    __resetState() {
        this.setState({
            time_spent: 0,
            type: 'type',
            date: '',
            valid: false
        });
    }

    /**
     * Validate Workout and allow submition when
     * everything is ok.
     *
     * @return void
     * @private
     */
    __validate() {
        if(
            this.state.time_spent != 0 &&
            this.state.type != 'type' &&  
            this.__isValidDate(this.state.date)
        )
        {
            this.setState({valid: true});
        }
        else
        {
            this.setState({valid: false});
        }
    }

    /**
     * Validate the informed date.
     *
     * @return bool
     * @private
     */
    __isValidDate(date)
    {
        return date.length == 10 && Moment(date, 'DD/MM/YYYY').isValid();
    }
}

export default WorkoutForm;