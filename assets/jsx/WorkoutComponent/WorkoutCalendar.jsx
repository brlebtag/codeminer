import React from 'react';
import Moment from 'moment/moment';

/**
 * WorkoutCalendar Class
 *
 * This class outputs the WorkoutForm's Calendar
 *
 * @author Bruno Lebtag <brlebtag@gmail.com>
 * @version 1.0.0
 */
class WorkoutCalendar extends React.Component {

    /**
     * Class Constructor 
     *
     * @param mixed props immutable states object.
     * @return void
     * @public
     */
    constructor(props) {
        super(props);

        // Fixing `this` variable in es6 class 
        this.handleDateChanged = this.handleDateChanged.bind(this);
        this.todayDate = this.todayDate.bind(this);
    }

    /**
     * Handle <input> tag `date` events
     *
     * @param mixed event event object.
     * @return void
     * @public
     */
    handleDateChanged(event) {
        // format: 
        // dd/mm/yyyy 
        // 10 characters (at most)
        // only numbers and slash
        this.props.onDateChanged(event.target.value.replace(/[^0-9\/]/, '').substr(0, 10));
    }

    /**
     * Insert Today Date into calendar
     *
     * @return void
     * @public
     */
     todayDate()
     {
        this.props.onDateChanged(Moment().format('DD/MM/YYYY'));
     }

     /**
     * Force MDL to update its states...
     *
     * @return void
     * @public
     */
     componentDidUpdate()
     {
        document.querySelector('.mdl-js-textfield').MaterialTextfield.updateClasses_();
     }

    /**
     * Render the UI Object.
     *  
     * @return mixed
     * @public
     */
    render() {
        return(
            <div className="workout-calendar">
                <div className="centralized wider">
                    <div className="column">
                        <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input value={this.props.value} onChange={this.handleDateChanged} className="mdl-textfield__input" type="text"/>
                            <label className="mdl-textfield__label">Date (e.g. 13/07/1991)</label>
                        </div>
                    </div>
                    <div className="column">
                        <button title="Today Date" onClick={this.todayDate} className="mdl-button mdl-js-button mdl-button--icon">
                            <i className="material-icons">event</i>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default WorkoutCalendar;