import React from 'react';

/**
 * WorkoutResult Class
 *
 * This class outputs the WorkoutComponent's result.
 *
 * @author Bruno Lebtag <brlebtag@gmail.com>
 * @version 1.0.0
 */
class WorkoutResult extends React.Component {

    /**
     * Class Constructor 
     *
     * @param mixed props immutable states object.
     * @return void
     * @public
     */
    constructor(props) {
        super(props);
    }

    /**
     * Render the UI Object.
     *  
     * @return mixed
     * @public
     */
    render() {
        return(
            <h2 className="text-center">{this.props.result} hour(s) of exercises</h2>
        );
    }
}

export default WorkoutResult;